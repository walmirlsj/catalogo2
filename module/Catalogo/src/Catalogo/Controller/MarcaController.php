<?php

namespace Catalogo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Filter\StringTrim;
use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Stdlib\StringUtils;

class MarcaController extends AbstractActionController
{
    private $em;
    private $entity;
    private $service;
    private $controller;
    private $route;

    public function __construct()
    {
        $this->entity = "Catalogo\Entity\Marca";
        $this->service = "Catalogo\Service\Marca";
        $this->form = "Catalogo\Form\Marca";
        $this->controller = "marca";
        $this->route = "catalogo/default";
    }

    public function indexAction()
    {
        $marca = $this->getEvent()->getRouteMatch()->getParam('marca', 0);
        $refid = $this->getEvent()->getRouteMatch()->getParam('refid', 0);

        $list = $this->getEm()
            ->getRepository($this->entity)
            ->findAll();

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(10);

        $messages = array(
            "Messages" => $this->flashMessenger()->setNamespace('Marca\Current')->getCurrentMessages(),
            "Success" => $this->flashMessenger()->setNamespace('Marca\Success')->getCurrentSuccessMessages(),
            "Warning" => $this->flashMessenger()->setNamespace('Marca\Warning')->getCurrentWarningMessages(),
            "Info" => $this->flashMessenger()->setNamespace('Marca\Info')->getCurrentInfoMessages(),
            "Danger" => $this->flashMessenger()->setNamespace('Marca\Danger')->getCurrentErrorMessages()
        );

        return new ViewModel(array('data' => $paginator, 'page' => $page, 'messages' => $messages));
    }


    public function inserirAction()
    {
        $form = $this->getServiceLocator()->get($this->form);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $request->getPost()->toArray();


                $service = $this->getServiceLocator()->get($this->service);
                $resp = $service->insert($data);


                if (!is_array($resp)) {
                    $this->flashMessenger()
                        ->setNamespace('Marca\Success')
                        ->addSuccessMessage("Marca inserida com sucesso");
                } else {
//                    $form->setData(array_fill_keys(array_keys((array)$data), ''));
                    $this->flashMessenger()
                        ->setNamespace($resp["namespace"])
                        ->addErrorMessage("Ops!!! Ocorreu um erro ao inserir marca, entre em contato com suporte." . '<br>' .
                            'Problema: ' . $resp["msg"]);
                }

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
//                $url = $this->getRequest()->getHeader('Referer')->getUri();
//                $this->redirect()->toUrl($url);

            }
        }

        $messages = array(
            "Messages" => $this->flashMessenger()->setNamespace('Marca\Current')->getCurrentMessages(),
            "Success" => $this->flashMessenger()->setNamespace('Marca\Success')->getCurrentSuccessMessages(),
            "Warning" => $this->flashMessenger()->setNamespace('Marca\Warning')->getCurrentWarningMessages(),
            "Info" => $this->flashMessenger()->setNamespace('Marca\Info')->getCurrentInfoMessages(),
            "Danger" => $this->flashMessenger()->setNamespace('Marca\Danger')->getCurrentErrorMessages()
        );

        $view = new ViewModel(array('form' => $form, 'messages' => $messages));
        $view->setTerminal($this->getRequest()->isXmlHttpRequest());
        return $view;
    }

    public function removerAction()
    {

//        $auth = $this->getServiceLocator()->get("AuthService");
//        $atleta = $this->CheckAtleta($auth);
//        if (!$atleta) {
//            $this->redirect()->toRoute("atleta/default", array("controller" => "perfil", "action" => "new"));
//        }

        $refid =  $this->params()->fromRoute('refid', 0);


        $service = $this->getServiceLocator()->get($this->service);
        $resp = $service->remove($refid);

        if (!is_array($resp)) {
            $this->flashMessenger()
                ->setNamespace('Marca\Success')
                ->addSuccessMessage("Registro da marca foi removido com sucesso!");
        } else {
            $this->flashMessenger()
                ->setNamespace($resp["namespace"])
                ->addErrorMessage("Ops!!! Ocorreu um erro ao remover o registro da marca, entre em contato com suporte." . '<br>' .
                    'Problema: ' . $resp["msg"]);
        }

        return $this->redirect()->toRoute('catalogo/default', array('controller' => 'marca'));
//        $url = $this->getRequest()->getHeader('Referer')->getUri();
//        $this->redirect()->toUrl($url);
    }

    public function editarAction()
    {
        $form = $this->getServiceLocator()->get($this->form);

        $request = $this->getRequest();
        $refid = $this->params()->fromRoute('refid', 0);
        $repository = $this->getEm()->getRepository($this->entity);
        $entity = $repository->find($refid);

        if (!$request->isPost() && $entity && $refid) {
            $form->setData($entity->toArray());
        }

        if ($request->isPost()) {

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();
//                print_r($data);die;
                $service = $this->getServiceLocator()->get($this->service);
                $resp = $service->update($data);

                if (!is_array($resp)) {
                    $this->flashMessenger()
                        ->setNamespace('Marca\Success')
                        ->addSuccessMessage("As informações do marca foram atualizadas com sucesso!");
                } else {
                    $this->flashMessenger()
                        ->setNamespace($resp["namespace"])
                        ->addErrorMessage("Ops!!! Ocorreu um erro ao editar marca, entre em contato com suporte." . '<br>' .
                            'Problema: ' . $resp["msg"]);
                }

                return $this->redirect()->toRoute($this->route, array('controller' => $this->controller));
//                $url = $this->getRequest()->getHeader('Referer')->getUri();
//                $this->redirect()->toUrl($url);

            }
        }

        $messages = array(
            "Messages" => $this->flashMessenger()->setNamespace('Marca\Current')->getCurrentMessages(),
            "Success" => $this->flashMessenger()->setNamespace('Marca\Success')->getCurrentSuccessMessages(),
            "Warning" => $this->flashMessenger()->setNamespace('Marca\Warning')->getCurrentWarningMessages(),
            "Info" => $this->flashMessenger()->setNamespace('Marca\Info')->getCurrentInfoMessages(),
            "Danger" => $this->flashMessenger()->setNamespace('Marca\Danger')->getCurrentErrorMessages()
        );

        $view = new ViewModel(array('form' => $form, 'messages' => $messages, 'refid' => $refid));
        $view->setTerminal($this->getRequest()->isXmlHttpRequest());
        return $view;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEm()
    {
        if (null === $this->em)
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        return $this->em;
    }


}

