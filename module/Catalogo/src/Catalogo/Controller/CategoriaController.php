<?php

namespace Catalogo\Controller;

use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Filter\StringTrim;
use Zend\Paginator\Paginator,
    Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Stdlib\StringUtils;
use Zend\View\Model\JsonModel;

class CategoriaController extends AbstractActionController
{
    private $em;
    private $entity;
    private $service;
    private $controller;
    private $route;

    /* configuração generica */
    private $form_class_default;
    private $style_panel_default;
    private $class_style;

    private $actionInserir;
    private $actionIcon_inserir;

    private $actionEditar;
    private $actionIcon_editar;

    public function __construct()
    {
        $this->entity = "Catalogo\Entity\Categoria";
        $this->service = "Catalogo\Service\Categoria";
        $this->form = "Catalogo\Form\Categoria";
        $this->controller = "Categoria";
        $this->route = "catalogo/default";

        $this->style_panel_default = "panel-crud-dark";
        $this->form_class_default = 'form-inline';

        $this->actionInserir = "Adicionar " . $this->controller;
        $this->actionIcon_inserir = 'glyphicon-plus';

        $this->actionEd = "Editar " . $this->controller;
        $this->actionIcon_editar = 'glyphicon-pencil';

        $this->class_style = 'panel-action-form';

    }

    public function indexAction()
    {
        $categoria = $this->getEvent()->getRouteMatch()->getParam('categoria', 0);
        $refid = $this->getEvent()->getRouteMatch()->getParam('refid', 0);

        $list = $this->getEm()
            ->getRepository($this->entity)
            ->findAll();

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($list));
        $paginator->setCurrentPageNumber($page)
            ->setDefaultItemCountPerPage(10);

        $messages = array(
            "Messages" => $this->flashMessenger()->setNamespace('Categoria\Current')->getCurrentMessages(),
            "Success" => $this->flashMessenger()->setNamespace('Categoria\Success')->getCurrentSuccessMessages(),
            "Warning" => $this->flashMessenger()->setNamespace('Categoria\Warning')->getCurrentWarningMessages(),
            "Info" => $this->flashMessenger()->setNamespace('Categoria\Info')->getCurrentInfoMessages(),
            "Danger" => $this->flashMessenger()->setNamespace('Categoria\Danger')->getCurrentErrorMessages()
        );
        $view = new ViewModel(array('data' => $paginator, 'page' => $page, 'messages' => $messages));
        $view->setTerminal($this->getRequest()->isXmlHttpRequest());
        return $view;
    }


    public function inserirAction()
    {
        $form = $this->getServiceLocator()->get($this->form);

        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $service = $this->getServiceLocator()->get($this->service);
                $resp = $service->insert($data);

                if (!is_array($resp)) {
                    $this->flashMessenger()
                        ->setNamespace('Categoria\Success')
                        ->addSuccessMessage("Categoria inserida com sucesso");
                } else {
//                    $form->setData(array_fill_keys(array_keys((array)$data), ''));
                    $this->flashMessenger()
                        ->setNamespace($resp["namespace"])
                        ->addErrorMessage("Ops!!! Ocorreu um erro ao inserir categoria, entre em contato com suporte." . '<br>' .
                            'Problema: ' . $resp["msg"]);
                }

                $messages = array(
                    "Messages" => $this->flashMessenger()->setNamespace('Categoria\Current')->getCurrentMessages(),
                    "Success" => $this->flashMessenger()->setNamespace('Categoria\Success')->getCurrentSuccessMessages(),
                    "Warning" => $this->flashMessenger()->setNamespace('Categoria\Warning')->getCurrentWarningMessages(),
                    "Info" => $this->flashMessenger()->setNamespace('Categoria\Info')->getCurrentInfoMessages(),
                    "Danger" => $this->flashMessenger()->setNamespace('Categoria\Danger')->getCurrentErrorMessages()
                );
                if ($request->isXmlHttpRequest()) {
                    $data = array('error' => $this->url()->fromRoute($this->route, array('controller' => $this->controller)));
                    $response = $this->getResponse();
                    $response->setStatusCode(200);
                    $response->setContent(json_encode($data));

                    $headers = $response->getHeaders();
                    $headers->addHeaderLine('Content-Type', 'application/json');

                    return $response;

                }

            }
        }

        $messages = array(
            "Messages" => $this->flashMessenger()->setNamespace('Categoria\Current')->getCurrentMessages(),
            "Success" => $this->flashMessenger()->setNamespace('Categoria\Success')->getCurrentSuccessMessages(),
            "Warning" => $this->flashMessenger()->setNamespace('Categoria\Warning')->getCurrentWarningMessages(),
            "Info" => $this->flashMessenger()->setNamespace('Categoria\Info')->getCurrentInfoMessages(),
            "Danger" => $this->flashMessenger()->setNamespace('Categoria\Danger')->getCurrentErrorMessages()
        );

        $route = $this->url()->fromRoute($this->route, array('controller' => $this->controller, 'action' => 'inserir'));

        $view = new ViewModel(array(
            'form' => $form,
            'messages' => $messages,
            'action' => $this->actionInserir,
            'route' => $route,
            'style_panel' => $this->style_panel_default,
            'class_style' => $this->class_style,
            'actionIcon' => $this->actionIcon_inserir
        ));
        $view->setTerminal($this->getRequest()->isXmlHttpRequest());
        return $view;
    }

    public function editarAction()
    {

        $refid = $this->params()->fromRoute('refid', 0);
        if (!$refid) {
            return $this->redirect()->toRoute($this->route, array('controller' => $this->controller, 'action' => 'inserir'));
        }

        $action = "Editar Categoria";

        $form = $this->getServiceLocator()->get($this->form);

        $request = $this->getRequest();


        $repository = $this->getEm()->getRepository($this->entity);
        $entity = $repository->find($refid);

        if (!$request->isPost() && $entity && $refid) {
            $form->setData($entity->toArray());
        }

        if ($request->isPost()) {

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();
//                print_r($data);die;
                $service = $this->getServiceLocator()->get($this->service);
                $resp = $service->update($data);

                if (!is_array($resp)) {
                    $this->flashMessenger()
                        ->setNamespace('Categoria\Success')
                        ->addSuccessMessage("As informações do categoria foram atualizadas com sucesso!");
                } else {
                    $this->flashMessenger()
                        ->setNamespace($resp["namespace"])
                        ->addErrorMessage("Ops!!! Ocorreu um erro ao editar categoria, entre em contato com suporte." . '<br>' .
                            'Problema: ' . $resp["msg"]);
                }

                $messages = array(
                    "Messages" => $this->flashMessenger()->setNamespace('Categoria\Current')->getCurrentMessages(),
                    "Success" => $this->flashMessenger()->setNamespace('Categoria\Success')->getCurrentSuccessMessages(),
                    "Warning" => $this->flashMessenger()->setNamespace('Categoria\Warning')->getCurrentWarningMessages(),
                    "Info" => $this->flashMessenger()->setNamespace('Categoria\Info')->getCurrentInfoMessages(),
                    "Danger" => $this->flashMessenger()->setNamespace('Categoria\Danger')->getCurrentErrorMessages()
                );
                if ($request->isXmlHttpRequest()) {
                    $data = array('error' => $this->url()->fromRoute($this->route, array('controller' => $this->controller)));
                    $response = $this->getResponse();
                    $response->setStatusCode(200);
                    $response->setContent(json_encode($data));

                    $headers = $response->getHeaders();
                    $headers->addHeaderLine('Content-Type', 'application/json');

                    return $response;

                }

            }
        }

        $messages = array(
            "Messages" => $this->flashMessenger()->setNamespace('Categoria\Current')->getCurrentMessages(),
            "Success" => $this->flashMessenger()->setNamespace('Categoria\Success')->getCurrentSuccessMessages(),
            "Warning" => $this->flashMessenger()->setNamespace('Categoria\Warning')->getCurrentWarningMessages(),
            "Info" => $this->flashMessenger()->setNamespace('Categoria\Info')->getCurrentInfoMessages(),
            "Danger" => $this->flashMessenger()->setNamespace('Categoria\Danger')->getCurrentErrorMessages()
        );

        $route = $this->url()->fromRoute($this->route, array('controller' => $this->controller, 'action' => 'editar', 'refid' => $refid));
        $view = new ViewModel(array(
            'form' => $form,
            'messages' => $messages,
            'refid' => $refid,
            'action' => $action,
            'route' => $route,
            'style_panel' => $this->style_panel_default,
            'class_style' => $this->class_style,
            'actionIcon' => $this->actionIcon_editar
        ));
        $view->setTerminal($this->getRequest()->isXmlHttpRequest());
        return $view;
    }

    public function removerAction()
    {

//        $auth = $this->getServiceLocator()->get("AuthService");
//        $atleta = $this->CheckAtleta($auth);
//        if (!$atleta) {
//            $this->redirect()->toRoute("atleta/default", array("controller" => "perfil", "action" => "new"));
//        }

        $refid = $this->params()->fromRoute('refid', 0);


        $service = $this->getServiceLocator()->get($this->service);
        $resp = $service->remove($refid);

        if (!is_array($resp)) {
            $this->flashMessenger()
                ->setNamespace('Categoria\Success')
                ->addSuccessMessage("Registro da categoria foi removido com sucesso!");
        } else {
            $this->flashMessenger()
                ->setNamespace($resp["namespace"])
                ->addErrorMessage("Ops!!! Ocorreu um erro ao remover o registro da categoria, entre em contato com suporte." . '<br>' .
                    'Problema: ' . $resp["msg"]);
        }

        return $this->redirect()->toRoute('catalogo/default', array('controller' => 'categoria'));
//        $url = $this->getRequest()->getHeader('Referer')->getUri();
//        $this->redirect()->toUrl($url);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEm()
    {
        if (null === $this->em)
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        return $this->em;
    }


}

