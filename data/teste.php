<?php

 return array (
  'Visitante' => 
  array (
    'Application::Index' => 
    array (
      0 => 'Index',
    ),
    'Catalogo::Index' => 
    array (
      0 => 'Index',
      1 => 'Detalhe',
    ),
    'Catalogo::Veiculo' => 
    array (
      0 => 'Detalhe',
    ),
    'User::Index' => 
    array (
      0 => 'Activate',
      1 => 'Register',
      2 => 'Recover',
    ),
    'User::Auth' => 
    array (
      0 => 'Index',
    ),
  ),
  'Admin' => 
  array (
    'Acl::Index' => 
    array (
      0 => 'Index',
    ),
    'Acl::Privilege' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
    'Acl::Resource' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
    'Acl::Role' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
    'Catalogo::Veiculo' => 
    array (
      0 => 'Remover',
    ),
    'User::Manager' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
  ),
  'Funcionario' => 
  array (
    'Catalogo::Categoria' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
    'Catalogo::Marca' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
    'Catalogo::Teste' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
      3 => 'Remover',
    ),
    'Cliente::Manager' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Remover',
    ),
  ),
  'User' => 
  array (
    'Catalogo::Veiculo' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
      2 => 'Editar',
    ),
    'Cliente::Index' => 
    array (
      0 => 'Index',
      1 => 'Inserir',
    ),
    'Cliente::Manager' => 
    array (
      0 => 'Editar',
    ),
    'User::Auth' => 
    array (
      0 => 'Logout',
    ),
  ),
);
