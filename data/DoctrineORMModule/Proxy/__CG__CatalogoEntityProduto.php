<?php

namespace DoctrineORMModule\Proxy\__CG__\Catalogo\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Produto extends \Catalogo\Entity\Produto implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'id', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'modelo', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'anoModelo', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'cor', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'opcionais', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'obs', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'price', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'icone', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'path', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'stocked', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'createdAt', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'updatedAt', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'categoria', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'cliente', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'marca');
        }

        return array('__isInitialized__', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'id', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'modelo', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'anoModelo', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'cor', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'opcionais', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'obs', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'price', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'icone', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'path', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'stocked', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'createdAt', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'updatedAt', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'categoria', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'cliente', '' . "\0" . 'Catalogo\\Entity\\Produto' . "\0" . 'marca');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Produto $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function toArray()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toArray', array());

        return parent::toArray();
    }

    /**
     * {@inheritDoc}
     */
    public function setArray(array $data = array (
))
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setArray', array($data));

        return parent::setArray($data);
    }

    /**
     * {@inheritDoc}
     */
    public function getArrayCopy()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getArrayCopy', array());

        return parent::getArrayCopy();
    }

    /**
     * {@inheritDoc}
     */
    public function exchangeArray()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'exchangeArray', array());

        return parent::exchangeArray();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId($id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', array($id));

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getModelo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getModelo', array());

        return parent::getModelo();
    }

    /**
     * {@inheritDoc}
     */
    public function setModelo($modelo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setModelo', array($modelo));

        return parent::setModelo($modelo);
    }

    /**
     * {@inheritDoc}
     */
    public function getAnoModelo()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAnoModelo', array());

        return parent::getAnoModelo();
    }

    /**
     * {@inheritDoc}
     */
    public function setAnoModelo($anoModelo)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAnoModelo', array($anoModelo));

        return parent::setAnoModelo($anoModelo);
    }

    /**
     * {@inheritDoc}
     */
    public function getCor()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCor', array());

        return parent::getCor();
    }

    /**
     * {@inheritDoc}
     */
    public function setCor($cor)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCor', array($cor));

        return parent::setCor($cor);
    }

    /**
     * {@inheritDoc}
     */
    public function getOpcionais()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOpcionais', array());

        return parent::getOpcionais();
    }

    /**
     * {@inheritDoc}
     */
    public function setOpcionais($opcionais)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOpcionais', array($opcionais));

        return parent::setOpcionais($opcionais);
    }

    /**
     * {@inheritDoc}
     */
    public function getObs()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getObs', array());

        return parent::getObs();
    }

    /**
     * {@inheritDoc}
     */
    public function setObs($obs)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setObs', array($obs));

        return parent::setObs($obs);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrice', array());

        return parent::getPrice();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrice($price)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrice', array($price));

        return parent::setPrice($price);
    }

    /**
     * {@inheritDoc}
     */
    public function getIcone()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIcone', array());

        return parent::getIcone();
    }

    /**
     * {@inheritDoc}
     */
    public function setIcone($icone)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIcone', array($icone));

        return parent::setIcone($icone);
    }

    /**
     * {@inheritDoc}
     */
    public function getPath()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPath', array());

        return parent::getPath();
    }

    /**
     * {@inheritDoc}
     */
    public function setPath($path)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPath', array($path));

        return parent::setPath($path);
    }

    /**
     * {@inheritDoc}
     */
    public function isStocked()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isStocked', array());

        return parent::isStocked();
    }

    /**
     * {@inheritDoc}
     */
    public function setStocked($stocked)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStocked', array($stocked));

        return parent::setStocked($stocked);
    }

    /**
     * {@inheritDoc}
     */
    public function getCategoria()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCategoria', array());

        return parent::getCategoria();
    }

    /**
     * {@inheritDoc}
     */
    public function setCategoria($categoria)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCategoria', array($categoria));

        return parent::setCategoria($categoria);
    }

    /**
     * {@inheritDoc}
     */
    public function getCliente()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCliente', array());

        return parent::getCliente();
    }

    /**
     * {@inheritDoc}
     */
    public function setCliente($cliente)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCliente', array($cliente));

        return parent::setCliente($cliente);
    }

    /**
     * {@inheritDoc}
     */
    public function getMarca()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMarca', array());

        return parent::getMarca();
    }

    /**
     * {@inheritDoc}
     */
    public function setMarca($marca)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMarca', array($marca));

        return parent::setMarca($marca);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', array());

        return parent::setUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', array());

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', array());

        return parent::setCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', array());

        return parent::getCreatedAt();
    }

}
